package edu.java.spring;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Hello world!
 *
 */
public class App {
	public static void main(String[] args) {
		System.out.println("Start.... !");
		// ApplicationContext context = new
		// ClassPathXmlApplicationContext("beans.xml");
		// HelloClazz obj = (HelloClazz)context.getBean("helloJavaClazz");
		// obj.printMessage();

		// AnnotationConfigApplicationContext ctx = new
		// AnnotationConfigApplicationContext();
		// ctx.register(AppConfig.class);
		// ctx.refresh();
		// HelloClazz myBean = (HelloClazz) ctx.getBean("bean2");
		// myBean.printMessage();

		// ApplicationContext context = new
		// ClassPathXmlApplicationContext("beans.xml");
		// HelloClazz obj = (HelloClazz)context.getBean("helloJavaClazz");
		// obj.printMessage();

		// ApplicationContext context = new
		// ClassPathXmlApplicationContext("beans.xml");

		// HelloClazz obj1 = (HelloClazz)context.getBean("helloJavaClazz");
		// obj1.printMessage();
		// System.out.println(obj1);

		// HelloClazz obj2 = (HelloClazz)context.getBean("helloJavaClazz");
		// obj2.printMessage();
		// System.out.println(obj2);

		// ApplicationContext context = new
		// ClassPathXmlApplicationContext("beans.xml");
		// HelloClazz obj = (HelloClazz)context.getBean("helloJavaClazz");
		// obj.printMessage();

		// AnnotationConfigApplicationContext ctx = new
		// AnnotationConfigApplicationContext();
		// ctx.register(AppConfig.class);
		// ctx.refresh();
		// HelloClazz myBean = (HelloClazz) ctx.getBean("bean2");
		// myBean.printMessage();

		// ApplicationContext context = new
		// ClassPathXmlApplicationContext("beans.xml");
		// HelloClazz obj = (HelloClazz)context.getBean("helloJavaClazz");
		// obj.printMessage();

		//ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
		//context.registerShutdownHook();
		//HelloClazz obj = (HelloClazz) context.getBean("helloJavaClazz2");
		//obj.printMessage();
		
		//HelloWorld hello = (HelloWorld) context.getBean("HelloWorld");
		//hello.sayHello();
		
		
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
		context.start();
		//HelloWorld hello = (HelloWorld) context.getBean("HelloWorld");
		//hello.sayHello();
		
		
		//JavaClazz javaclazz = (JavaClazz) context.getBean("jee01");
		//System.out.println("Map implement is "+ javaclazz.getStudents().getClass());
		//System.out.println("There are "+ javaclazz.getStudents().size() + " in the class");
		
		
		//HelloClazz clazz = (HelloClazz) context.getBean("helloJavaClazz");
		//System.out.println("Toltal classes is "+ clazz.getClazzes().size());
		
		
		HelloWorld hello = (HelloWorld) context.getBean("helloWorld");
		hello.sayHello();
		context.stop();
		
		context.close();

	}
}
