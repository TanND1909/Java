package edu.java.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;

public class HelloWorld {

	
	
	private String message;
	
	@Autowired(required=true)
	@Qualifier("helloJavaClazz2") // autowire for clazz
	private HelloClazz clazz;
	
	//public HelloWorld(String name, HelloClazz clazz) {
	//	message = "(From HelloWorld constructor: "+name +" : " + clazz.getMessage() + ")";
	//}

	public void sayHello() {
		clazz.printMessage();
		System.out.println("From to HelloWorld: " + message);
	}

	@Required
	public void setMessage(String message) {
		this.message = message;
	}

	public void setClazz(HelloClazz clazz) {
		this.clazz = clazz;
	}

}
