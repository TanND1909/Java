package edu.java.spring;

import java.util.List;

import org.springframework.beans.factory.DisposableBean;

public class HelloClazz implements DisposableBean{
	private String message;
	private List<JavaClazz> clazzes;

	public List<JavaClazz> getClazzes() {
		return clazzes;
	}

	public void setClazzes(List<JavaClazz> clazzes) {
		this.clazzes = clazzes;
	}

	public HelloClazz() {
		message = "From Constructor: Say hello everyone !";
	}
	
	public HelloClazz(int person) {
		message = "From Constructor: Say hello to "+ person +" person(s) !";
	} 
	
	public HelloClazz(HelloClazz clazz) {
		message = clazz.message;
	} 
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		//this.message = message;
		this.message = "Call from Setter: " +message;
	}
	
	public void printMessage() {
		System.out.println("Your Meseage: "+ message );
	}
	
	@SuppressWarnings("unused")
	private void initMessage() {
		//message = "From init method: say hello bean !";
		System.out.println("Calling init method..." );
		message = "From init method: say hello bean !";
		
	}


	public void destroy() throws Exception {
		message = null;
		System.out.println("Meseage is null" );
		
	}
	
	//private void release() {
	//	message = null;
	//	System.out.println("Meseage is null" );
	//}
}
