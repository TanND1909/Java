package edu.java.spring.jdbc;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class JdbcApp {

	public static void main(String[] args) {
		
		System.out.println("Start...");
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
		StudentJdbcDAO jdbc = (StudentJdbcDAO) context.getBean("studentJdbcDAO");
		//jdbc.insert("Nguyen Van A", 20);
		//jdbc.insert("Nguyen Van B", 21);
		//jdbc.insert("Nguyen Van c", 23);
		
		//jdbc.updateAgeByName("Nguyen Duy Tan", 30);
		
		//jdbc.deleteAgeByName("Nguyen Van c");
		//jdbc.deleteAgeByName("Tran Thi A");
		
		//System.out.println("Total student is " + jdbc.totalRecord());
		
		//List<Student> students = jdbc.loadStudent("Nguyen Duy Tan");
		//for(Student student:students) {
		//	System.out.println(student);
		//}
		
		//students.add(new Student("Tran Thi A",17));
		//students.add(new Student("Le Van L",20));
		//students.add(new Student("Phan Thi Z",25));
		
		//int[] insertCounts = jdbc.add(students);
		
		//System.out.println("Total student in list is " + students.size());
		
		//for(int i=0; i<insertCounts.length;i++) {
		//	System.out.println("add record " +  i + " : " +(insertCounts[i] ==0?"failed":"success"));
		//}
		
		//jdbc.save("Tran Thi A", 23);
		//jdbc.save("Tran Thi A", "23dfd");
		
		System.out.println("Total student is " + jdbc.totalRecord());
		context.close();
		System.out.println("Done!");
	}

}
