package edu.java.spring.view;

import java.awt.Color;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.view.document.AbstractPdfView;

import com.itextpdf.text.Document;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfWriter;

import edu.java.spring.model.JavaClazz;
import edu.java.spring.model.Student;

public class PdfView extends AbstractPdfView {
	@Override
	protected void buildPdfDocument(Map model, Document doc, PdfWriter writer,
			HttpServletRequest request, HttpServletResponse response) {
		JavaClazz clazz = (JavaClazz) model.get("clazzObbj");
		table = new PdfPTable(3);
		table.setWidthPercentage(100.0f);
		table.setWidths(new float[] { 2.0f, 3.0f, 1.5f });
		table.setSpacingBefore(10);

		Font font = FontFactory.getFont(FontFactory.COURIER);
		font.setColor(Color.WHITE);// awt color

		PdfPCell cell = new PdfPCell();
		cell.setBackgroundColor(Color.CYAN);// awt color
		cell.setPadding(5);

	}
	// 5.14
	/*
	 * @Override protected void buildPdfDocument(Map model,Document
	 * doc,PdfWriter writer, HttpServletRequest request , HttpServletResponse
	 * response ) { JavaClazz clazz = (JavaClazz) model.get("clazzObbj"); table
	 * = new PdfPTable(3); table.setWidthPercentage(100.0f); table.setWidths(new
	 * float[] {2.0f,3.0f,1.5f}); table.setSpacingBefore(10);
	 * 
	 * Font font = FontFactory.getFont(FontFactory.COURIER);
	 * font.setColor(Color.WHITE);// awt color
	 * 
	 * PdfPCell cell = new PdfPCell(); cell.setPhrase(new Phrase("ID",font));
	 * cell.setBackgroundColor(Color.CYAN);// awt color cell.setPadding(5);
	 * 
	 * table.addCell(cell); // add Name and Age column table.completeRow();
	 * 
	 * 
	 * for(Student student:clazz.getStudents()) {
	 * table.addCell(String.valueOf(student.getId())); //add Name and Age
	 * table.completeRow(); } doc.add(table);
	 * 
	 * }
	 */

}
