package edu.java.spring.model;

import java.io.ByteArrayOutputStream;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.transform.dom.DOMSource;

import org.springframework.jdbc.core.RowMapper;

public class StudentMapper implements RowMapper<Student> {

	public Student mapRow(ResultSet rs, int rowNum) {
		Student student = new Student();
		try {
			student.setId(rs.getInt("id"));
			student.setName(rs.getString("name"));
			student.setAge(rs.getInt("age"));
		} catch (SQLException e) {
			System.out.println("Error " + e);
		}

		return student;
	}
	public static DOMSource clazzToDomSource(JavaClazz clazz) {
		JAXBContext jaxbContext = JAXBContext.newInstance(JavaClazz.class);
		jaxbMarshaller = jaxbContext.createMarshaller();
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,true);
		outputStream = new ByteArrayOutputStream();
		jaxbMarshaller.marshal(clazz,outputStream);
		
		factory = DocumentBulderFactory.newlnstance();
		builber = factory.newDocumentBuilder();
		document = builder.parse(new ByteArrayOutputStream(outputStream.toByteArray()));
		
		return new DOMSource(document);
	}
}
