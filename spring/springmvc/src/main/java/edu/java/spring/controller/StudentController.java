package edu.java.spring.controller;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import edu.java.spring.dao.StudentDAO;
import edu.java.spring.model.JavaClazz;
import edu.java.spring.model.Student;
import edu.java.spring.model.StudentMapper;

@Controller
public class StudentController {
	@Autowired
	private StudentDAO studentDAO;

	@RequestMapping(value = "/student/form", method = RequestMethod.GET)
	public ModelAndView student() {
		return new ModelAndView("/student/StudentForm", "command",
				new Student());
	}

	@RequestMapping(value = "/student/add", method = RequestMethod.POST)
	public ModelAndView addStudent(
			@Valid @ModelAttribute("command") Student student,
			BindingResult result) {
		ModelAndView model = null;
		if (result.hasErrors()) {
			model = new ModelAndView("/student/StudentForm", "command", student);
			model.addObject("errors", result);
			return model;
		}
		
		if(student.getId()>0) {
			//System.out.println("\n\n Edit student" + student.getId() + "\n\n");
			studentDAO.update(student);
		}else {
			studentDAO.insert(student);
		}

		studentDAO.insert(student);
		// model = new ModelAndView("/student/StudentView");
		model = new ModelAndView("redirect:/student/list");
		model.getModelMap().put("name", student.getName());
		model.getModelMap().put("age", student.getAge());
		return model;
	}

	@RequestMapping(value = "/student/list", method = RequestMethod.GET)
	public ModelAndView listStudents(@RequestParam(value="q",required=false) String query) {
		ModelAndView model = new ModelAndView("StudentList");
		model.addObject("students", studentDAO.listStudents());
		return model;
	}

	@RequestMapping(value = "/student/view/{id}")
	public String loadStudent(@PathVariable Integer id , ModelMap model) {
		Student student = new Student();
		
		student = studentDAO.loadStudent(id);
		model.put("id", student.getId());
		model.put("name", student.getName());
		model.put("age", student.getAge());
		
		return "StudentView";
	}
	
	@RequestMapping(value = "/student/edit/{id}", method = RequestMethod.GET)
	public ModelAndView editStudent(@PathVariable Integer id) {
		Student student = new Student();
		student = studentDAO.loadStudent(id);
		ModelAndView model = new ModelAndView("../StudentForm","command",student);
		model.getModelMap().put("id", student.getId());
		model.getModelMap().put("name", student.getName());
		model.getModelMap().put("age", student.getAge());
		return model;
	}
	
	@RequestMapping(value = "/student/delete/{id}")
	public String delStudent(@PathVariable Integer id) {
		studentDAO.delete(id);
		return "StudentList";
	}
	
	@RequestMapping(value = "/student/xml", MediaType.APPLICATION_XML_VALUE)
	public @ResponseBody JavaClazz viewlnXML() {
		return studentDAO.getJavaClazz();
	}
	
	@RequestMapping(value = "/student/json", MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody JavaClazz viewlnJSON() {
		return studentDAO.getJavaClazz();
	}
	
	@RequestMapping(value="/xsl/view")
	public ModelAndView viewXSLT() {
		JavaClazz clazz = studentDAO.getJavaClazz();
		ModelAndView model = new ModelAndView("ClazzView");
		model.getModelMap().put("data", StudentMapper.clazzToDomSource(clazz));
		return model;
	}
	
	@RequestMapping(value="/xsl/view2",produces="application/xslt")
	public ModelAndView viewXslt2() {
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("clazzObj", studentDAO.getClass());
		return new ModelAndView("ClazzView",map);
	}
	
	@RequestMapping(value="/student/pdf",produces="application/pdf")
	public ModelAndView viewPdf() {
		JavaClazz clazz = studentDAO.getJavaClazz();
		return new ModelAndView("pdfView","clazzObj",clazz);
	}
	
	@RequestMapping(value="/student/report",produces="application/pdf")
	public ModelAndView viewReport() {
		students = studentDAO.listStudents();
		
		JRDataSource dataSource = new JRBeanC
	}
	
}
