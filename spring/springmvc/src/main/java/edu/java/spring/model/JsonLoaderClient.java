package edu.java.spring.model;


import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

public class JsonLoaderClient {

	public static void main(String[] args) {
		try {
			URL url= new URL("http://dantri.com.vn/");
			URLConnection connection= url.openConnection();
			connection.addRequestProperty("accept", "Application/json");
			InputStream stream= connection.getInputStream();
			
			int read;
			byte []bytes= new byte[4*1024];
			
			while ((read=stream.read(bytes))!=-1) {
				System.out.print(new String(bytes,0,read) + "\n");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
