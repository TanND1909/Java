package edu.java.spring.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
 
@Controller
@RequestMapping("/")
public class HelloWorldController {
	
	private String url = "http://localhost:8080/springmvc";
 
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView sayHome() {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("index");
		mav.addObject("message", "Hello Java Clazz! this is page index");
		mav.addObject("url", url);
		return mav;
    }
 
    @RequestMapping(value="/welcome", method = RequestMethod.GET)
    public ModelAndView sayWelcome() {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("welcome");
		mav.addObject("message", "Hello Java Clazz! this is page welcome");
		return mav;
    }
    @RequestMapping(value="/info", method = RequestMethod.GET)
    public ModelAndView sayInfo() {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("info");
		mav.addObject("message", "Hello Java Clazz! this is page info");
		return mav;
    }
 
}