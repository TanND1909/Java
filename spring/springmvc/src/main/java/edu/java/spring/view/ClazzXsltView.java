package edu.java.spring.view;

import java.util.Map;

import org.springframework.web.servlet.view.xslt.XsltView;

import edu.java.spring.model.StudentMapper;

public class ClazzXsltView  extends XsltView{
	protected Source locateSource model) {
		JavaClazz clazz = (JavaClazz) model.get("clazzObj");
		return StudentMapper.clazzToDomSource(clazz);
	}
}
