<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
	<head>
		<title> Student List</title>
	</head>
	<body>
		<h2>List of Students</h2>
		
		<table border="1">
			<tr>
				<form:form method="GET" action="list">
					<td colspan="4"><input type="text" name="q" size="30"/></td>/
					<td><input type="submit" value="Submit"/></td>
				</form:form>
			</tr>
			<tr>
				<td>Id</td> <td>Name</td> <td>Age</td>
			</tr>
			<c:forEach items ="${students}" var="student">
				<tr>
					<td>${student.id}</td>
					<td><a href="view/${student.id}">${student.getName()}</a></td>
					<td>${student.getAge()}</td>
					<td><a href="edit/${student.id}">Edit</a></td>
					<td><a href="delete/${student.id}">Del</a></td>
				</tr>
			</c:forEach>
		</table>
	</body>
</html>