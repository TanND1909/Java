<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Hello World page</title>
<style>
a {
	color: red;
}
</style>
</head>
<body>
	<h2>Message : ${message}</h2>
	<h2>URL : ${url}</h2>
	<div>
		<h2>
			<a href="${url}/">Home</a>
		</h2>
		<h2>
			<a href="${url}/welcome">Welcome</a>
		</h2>
		<h2>
			<a href="${url}/info">Info</a>
		</h2>
		<h2>
			<a href="${url}/student/form">StudentForm</a>
		</h2>
	</div>
</body>
</html>